ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"
libraryDependencies += "org.springframework.kafka" % "spring-kafka" % "3.0.4"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter" % "3.0.4"
libraryDependencies += "org.springframework.boot" % "spring-boot-starter-test" % "3.0.4" % Test
lazy val root = (project in file("."))
  .settings(
    name := "SpringScalaKafka"
  )
