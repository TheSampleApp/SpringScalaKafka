package com.the.sample.app.config

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core._

import java.util


@EnableKafka
@Configuration
class KafkaConfiguration {
  def producerFactory(bootstrapAddress: String): ProducerFactory[String, String] = {
    val configProps = new util.HashMap[String, AnyRef]
    configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress)
    configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer])
    configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer])
    new DefaultKafkaProducerFactory[String, String](configProps)
  }

  @Bean def kafkaTemplate(@Value("${kafka.bootstrap.address}") bootstrapAddress: String) = new KafkaTemplate[String, String](producerFactory(bootstrapAddress))

  def consumerFactory(bootstrapAddress: String, consumerGroup: String): ConsumerFactory[String, String] = {
    val props = new util.HashMap[String, AnyRef]
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress)
    props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroup)
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
    new DefaultKafkaConsumerFactory[String, String](props)
  }

  @Bean def kafkaListenerContainerFactory(@Value("${kafka.bootstrap.address}") bootstrapAddress: String, @Value("${kafka.consumer.group}") consumerGroup: String): ConcurrentKafkaListenerContainerFactory[String, String] = {
    val factory = new ConcurrentKafkaListenerContainerFactory[String, String]
    factory.setConsumerFactory(consumerFactory(bootstrapAddress, consumerGroup))
    factory
  }
}
