package com.the.sample.app.consumer

import org.springframework.kafka.annotation.KafkaListener

class EventListener {
  @KafkaListener(topics = Array("${kafka.topic.name}"), groupId = "${kafka.consumer.group}")
  def onEvent(eventMessage: String): Unit = {
    //process the event
  }
}
