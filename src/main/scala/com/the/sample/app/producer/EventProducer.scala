package com.the.sample.app.producer

import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

trait EventProducer {
  def produce(eventKey: String, eventValue: String): Unit
}

@Component
class EventProducerImpl(kafkaTemplate: KafkaTemplate[String, String],
                        @Value("${kafka.topic.name}") val topicName: String) extends EventProducer {
  override def produce (eventKey: String, eventValue: String): Unit =
    kafkaTemplate.send (topicName, eventKey, eventValue)
}